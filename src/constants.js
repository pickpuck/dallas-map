const currentHref = window.location.href.split('/')
currentHref.pop()

export const DATA_URL = currentHref.join('/') + '/locationData.json'
export const COMPANY_NAME = 'Dallas Group of America, Inc.'
export const BREAKPOINT = 600
export const DEFAULT_CENTER = {
    lat: 35,
    lng: -90
}
export const DEFAULT_ZOOM = 3
export const DETAIL_ZOOM = 8
export const GET_SIDEBAR_WIDTH = (layout) => ((layout.width > BREAKPOINT) ? 310 : 200)
