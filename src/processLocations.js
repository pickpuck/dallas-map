import { getIcon } from './sprite'

export default function processLocations (categories) {
    let {
        corporateLocations,
        salesOffices,
        salesStaff,
        packagingFacilities,
        manufacturingFacilities
    } = categories

    let counter = 0

    function addCategoryId (categoryId) {
        return function (location) {
            location.categoryId = categoryId
            location.icon = getIcon({categoryId})
            counter++
            location.id = counter
            return location
        }
    }

    return {
        corporate: {
            name: 'Corporate',
            id: 'corporate',
            locations: corporateLocations.map(addCategoryId('corporate'))
        },
        salesOffices: {
            name: 'Sales Offices',
            id: 'salesOffices',
            locations: salesOffices.map(addCategoryId('salesOffices'))
        },
        salesStaff: {
            name: 'Sales Staff',
            id: 'salesStaff',
            locations: salesStaff.map(addCategoryId('salesStaff'))
        },
        packagingFacility: {
            name: 'Packaging Facilities',
            id: 'packagingFacility',
            locations: packagingFacilities.map(addCategoryId('packagingFacility'))
        },
        manufacturingFacility: {
            name: 'Manufacturing Facilities',
            id: 'manufacturingFacility',
            locations: manufacturingFacilities.map(addCategoryId('manufacturingFacility'))
        },
    }
}
