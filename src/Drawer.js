import React, { Component } from 'react'
import { connect } from 'react-redux'
import AppBar from 'material-ui/AppBar'
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import { COMPANY_NAME } from './constants'
import * as colors from './colors'

export function drawerReducer (state, action) {
    switch (action.type) {
        case 'LOCATION_SELECTED':
            return Object.assign({}, state, { selectedLocation: action.marker })
        case 'DRAWER_OPEN':
            return Object.assign({}, state, { drawerIsOpen: true })
        case 'DRAWER_CLOSE':
            return Object.assign({}, state, { drawerIsOpen: false })
        default:
            return state
    }
}

export const Drawer = connect(
    state => ({
        data: state.selectedLocation || {}
    }),
    dispatch => ({
        _close: () => {
            dispatch({
                type: 'DRAWER_CLOSE'
            })
            dispatch({
                type: 'LOCATION_DESELECTED'
            })
        }
    })
)(class Drawer extends Component {
    constructor () {
        super()
        this.styles = {
            appBar: {
                boxShadow: 'none',
                height: '46px',
                paddingLeft: '0px',
                color: colors.textReverse,
                backgroundColor: colors.primary
            },
            closeButton: {
                paddingLeft:'10px',
                paddingRight:'10px',
                paddingTop:'5px'
            },
            title: {
                lineHeight:'46px',
                overflow: 'hidden',
                textOverflow:'ellipsis',
                whiteSpace: 'nowrap'
            },
            wrapper: {
                paddingLeft: '22px',
                paddingRight: '13px'
            }
        }
    }
    render () {
        let { data } = this.props
        return (
            <div>
                <AppBar showMenuIconButton={false} style={this.styles.appBar} titleStyle={{display:'none'}}>
                    <button style={this.styles.closeButton} onClick={this.props._close}>
                        <ArrowBack color={colors.textReverse} />
                    </button>
                    <div title={COMPANY_NAME} style={this.styles.title}>
                        {COMPANY_NAME}
                    </div>
                </AppBar>
                <div style={this.styles.wrapper}>
                    <div className="subhead">name</div>
                    <div>{data.name}</div>
                    <div className="subhead">description</div>
                    <div>
                        {data.description && data.description.map && data.description.map(paragraph => (
                            <div key={Math.random()} dangerouslySetInnerHTML={{__html: paragraph }} />
                        ))}
                    </div>
                </div>
            </div>
        )
    }
})
