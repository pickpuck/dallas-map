import React, { Component } from 'react'
import { connect, Provider } from 'react-redux'
import { withGoogleMap, GoogleMap, Marker } from 'react-google-maps'
import store from './store'
import { Drawer } from './Drawer'
import { Sidebar } from './Sidebar'
import injectTapEventPlugin from 'react-tap-event-plugin'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import DrawerContainer from 'material-ui/Drawer'
import AppBar from 'material-ui/AppBar'
import { DEFAULT_CENTER, DEFAULT_ZOOM, COMPANY_NAME, DATA_URL } from './constants'
import { getSidebarWidth } from './util'
import { debounce } from 'lodash'
import request from 'superagent'
import processLocations from './processLocations'
import * as colors from './colors'

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin()

const Map = connect(
    state => ({
        map: state.map
    }),
    dispatch => ({
        _onMarkerClick: (marker) => {
            dispatch({
                type: 'LOCATION_SELECTED',
                marker: marker
            })
            dispatch({
                type: 'DRAWER_OPEN',
                marker: location
            })
        },
        _onDrag: (map) => {
            let center = map.getCenter()
            dispatch({
                type: 'UPDATE_MAP_CENTER',
                center: {
                    lat: center.lat(),
                    lng: center.lng()
                },
                map: map
            })
            return true
        }
    })
)(withGoogleMap(props => {
    let { onMapLoad, defaultCenter, defaultZoom } = props
    let safeProps = {
        ref: onMapLoad,
        onDrag: () => props._onDrag(props.map),
        defaultCenter,
        defaultZoom,
        options: {
            streetViewControl: false,
            mapTypeControl: false,
            zoomControlOptions: {
                position: window.google.maps.ControlPosition['TOP_LEFT']
            }
        }
    }
    if (props.center) {
        safeProps.center = props.center
    }
    if (props.zoom) {
        safeProps.zoom = props.zoom
    }
    return(
        <GoogleMap {...safeProps}>
            {props.markers.map((marker, index) => {
                return (
                    <Marker
                        {...marker}
                        onClick={() => props._onMarkerClick(marker)}
                        key={index}
                    />)
            })}
        </GoogleMap>
)}))

const MapController = connect(
    state => ({
        drawerIsOpen: state.drawerIsOpen,
        sidebarIsOpen: state.sidebarIsOpen,
        locations: state.locations || {},
        selectedLocation: state.selectedLocation,
        center: state.center,
        zoom: state.zoom,
        layout: state.layout,
        sidebarWidth: getSidebarWidth(state)
    }),
    dispatch => ({
        _onMapLoad: map => {
            dispatch({
                type: 'MAP_LOADED',
                map: map
            })
        },
        _toggleSidebar: isOpen => {
            dispatch({
                type: isOpen ? 'SIDEBAR_CLOSE' : 'SIDEBAR_OPEN'
            })
        },
        _onLayout: () => {
            dispatch({
                type: 'KEEP_MAP_CENTERED'
            })

            let height = window.innerHeight
            let width = window.innerWidth

            dispatch({
                type: 'LAYOUT_DIMENSIONS_UPDATE',
                layout: {
                    height,
                    width
                }
            })
        },
        _getLocations: () => {
            request
                .get(DATA_URL)
                .end((error, response) => {
                    if (!error) {
                        dispatch({
                            type: 'LOCATION_DATA',
                            data: processLocations(response.body)
                        })
                    } else {
                        console.log(error)
                    }
                })
        }
    })
)(class MapController extends Component {
    constructor () {
        super()
        let appBarHeight = '46px'
        this.styles = {
            appBar: {
                boxShadow: 'none',
                height: appBarHeight,
                paddingLeft: '0px',
                color: colors.textReverse,
                backgroundColor: colors.appBar
            },
            appTitle: {
                fontSize: '16px',
                fontWeight: '500',
                lineHeight: '46px'
            },
            sidebar: {
                height: 'calc(100% - ' + appBarHeight + ')',
                marginTop: appBarHeight,
                backgroundColor: colors.sidebar,
                overflowX: 'hidden'
            },
            drawer: {
                backgroundColor: colors.drawer
            },
            mapContainer: {
                width: '100%'
            },
            map: {
                height: '100%',
                width: '100%'
            }
        }
    }
    componentWillMount () {
        this.props._getLocations()
    }
    componentDidMount () {
        this.props._onLayout()
        window.addEventListener('resize', debounce(this.props._onLayout, 10))
    }
    render () {
        let { locations, sidebarIsOpen } = this.props
        let locationMarkers = Object.keys(locations).map(locationKey => {
            let category = locations[locationKey]
            if (category.isHidden) {
                return []
            }
            return category.locations
        })
        let markers = [].concat.apply([], locationMarkers)
        return (
            <div>
                <AppBar showMenuIconButton={false} style={this.styles.appBar} titleStyle={{display:'none'}}>
                    <button className={'toggle-' + (sidebarIsOpen ? 'close' : 'open')} onClick={() => this.props._toggleSidebar(sidebarIsOpen)}>
                    </button>
                    <div style={this.styles.appTitle}>
                        {COMPANY_NAME}
                    </div>
                </AppBar>
                <div>
                    <DrawerContainer width={this.props.sidebarWidth} containerStyle={this.styles.sidebar}  open={this.props.sidebarIsOpen}>
                        <Sidebar />
                    </DrawerContainer>
                    <DrawerContainer width={this.props.sidebarWidth} containerStyle={this.styles.drawer} open={this.props.drawerIsOpen}>
                        <Drawer />
                    </DrawerContainer>
                    <Map
                      containerElement={
                        <div className="map-container-element" style={this.styles.mapContainer} />
                      }
                      mapElement={
                        <div style={this.styles.map} />
                      }
                      markers={ markers }
                      onMapLoad={this.props._onMapLoad}
                      defaultCenter={DEFAULT_CENTER}
                      defaultZoom={DEFAULT_ZOOM}
                      center={this.props.center}
                      zoom={this.props.zoom}
                    />
                </div>
            </div>
        )
    }
})

// Bootstrap App

class App extends Component {
  render() {
    return (
        <MuiThemeProvider>
            <Provider store={store}>
                <MapController />
            </Provider>
        </MuiThemeProvider>
    );
  }
}

export default App
