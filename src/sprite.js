const SPRITE_ORDER = ['corporate', 'salesOffices', 'salesStaff', 'packagingFacility', 'manufacturingFacility']
const SPRITE_WIDTH = 44
const SPRITE_SIZE = {
    width: SPRITE_WIDTH,
    height: SPRITE_WIDTH
}
const SPRITE_ANCHOR = {
    x: SPRITE_WIDTH / 2,
    y: (SPRITE_WIDTH / 2) + 22
}
const SPRITE_URL = 'sprite.png'

export const getIcon = ({categoryId, active}) => ({
    url: SPRITE_URL,
    size: SPRITE_SIZE,
    origin: {
        x: SPRITE_ORDER.indexOf(categoryId) * SPRITE_WIDTH,
        y: active ? SPRITE_WIDTH : 0
    },
    anchor: SPRITE_ANCHOR
})
