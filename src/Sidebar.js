import React, { Component } from 'react'
import { connect } from 'react-redux'
import Checkbox from 'material-ui/Checkbox'
import Divider from 'material-ui/Divider'
import { List, ListItem } from 'material-ui/List'
import { getSidebarWidth } from './util'
import { BREAKPOINT, DETAIL_ZOOM } from './constants'
import * as colors from './colors'
import FlatButton from 'material-ui/FlatButton'
import ArrowDownIcon from 'material-ui/svg-icons/hardware/keyboard-arrow-up'
import ArrowUpIcon from 'material-ui/svg-icons/hardware/keyboard-arrow-down'

export function locationsVisibilityReducer (state, action) {
    switch (action.type) {
        case 'LOCATIONS_VISIBILITY_CHANGE':
            let { id, isHidden } = action
            let newState = {}
            newState.locations = Object.assign({}, state.locations)
            newState.locations[id].isHidden = isHidden
            return Object.assign({}, state, newState)
        default:
            return state
    }
}

const Category = connect(
    state => ({
        layout: state.layout
    }),
    (dispatch, ownProps) => ({
        _handleVisibilityChange: (isHidden) => {
            let { id } = ownProps.category
            dispatch({
                type: 'LOCATIONS_VISIBILITY_CHANGE',
                id,
                isHidden
            })
        },
        _handleLocationClick: location => {
            dispatch({
                type: 'LOCATION_SELECTED',
                marker: location
            })
            dispatch({
                type: 'UPDATE_MAP_CENTER',
                center: location.position,
                map: ownProps.map,
                offsetX: ownProps.sidebarWidth / 2
            })
            dispatch({
                type: 'UPDATE_MAP_ZOOM',
                zoom: DETAIL_ZOOM
            })
            dispatch({
                type: 'DRAWER_OPEN',
                marker: location
            })
        }
    })
)(class Category extends Component {
    constructor (props) {
        super(props)
        this.state = {
            showAll: false,
            isHidden: false
        }
        this.styles = {
            container: {
                marginTop: '20px'
            },
            checkboxLabel: {
                fontWeight: '500',
                fontSize: '16px'
            },
            list: {
                paddingTop: '10px'
            },
            listItemContainer: {
                height: '24px',
                padding: '0 15px 2px 0'
            },
            listItem: {
                color: colors.bodyText,
                height: '20px',
                lineHeight: '20px',
                overflow: 'visible',
                paddingTop: '4px'
            },
            listItemText: {
                maxWidth: '185px',
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap'
            },
            fakeListItemText: {
                position: 'absolute',
                color: 'transparent',
                maxWidth: '185px',
                marginTop: '2px'
            },
            divider: {
                backgroundColor: colors.divider,
                marginTop: '22px',
                marginLeft: '45px'
            },
            toggleButton: {
                position: 'relative',
                left: '-6px',
                minWidth: '0',
                borderRadius: '100px',
                maxWidth: '35px',
                width: '35px',
                height: '35px',
                paddingTop: '6px',
                marginTop: '10px',
                zIndex: 1
            },
            arrow: {
                color: colors.disabled
            }
        }
    }
    _onCheck (isChecked) {
        let isHidden = !isChecked
        this.setState({isHidden: !isChecked})
        this.props._handleVisibilityChange(isHidden)
    }
    _toggle () {
        this.setState({showAll: !this.state.showAll})
    }
    render () {
        let { name, locations } = this.props.category
        let { isHidden, showAll } = this.state
        let isNarrow = (this.props.layout.width < BREAKPOINT)
        let maxCount = 4
        return (
            <li>
                <div style={this.styles.container}>
                    <Checkbox onCheck={(e, isChecked) => this._onCheck(isChecked)} labelStyle={Object.assign({}, this.styles.checkboxLabel, isHidden ? { color: colors.disabled } : {})} checked={!isHidden} iconStyle={{fill: isHidden ? colors.disabled : colors.checkbox}} label={name} />
                    <div style={(!isHidden && locations.length > maxCount) ? {} : { display: 'none' }}>
                        <FlatButton hoverColor="transparent" style={Object.assign({}, this.styles.toggleButton, isNarrow ? { display: 'none' } : { float: 'left' })} onClick={() => this._toggle()}>
                            {showAll ? (<ArrowDownIcon style={this.styles.arrow} />) : (<ArrowUpIcon style={this.styles.arrow} />)}
                        </FlatButton>
                    </div>
                    <div style={isHidden ? { display: 'none' } : {}}>
                        <List style={Object.assign(
                            {},
                            this.styles.list,
                            {
                                paddingLeft: (isNarrow) ? '0px' : '40px'
                            }
                            )}>
                                {locations.map((location, i) => (
                                    <div key={'div' + i} style={(showAll || i < maxCount) ? {} : { display: 'none' }}>
                                        <ListItem style={{zIndex: locations.length - i}}  hoverColor="transparent" innerDivStyle={this.styles.listItemContainer} onClick={() => this.props._handleLocationClick(location)} secondaryText={(
                                            <div style={this.styles.listItem}>
                                                <div className={'marker-icon marker-icon-' + location.categoryId} />
                                                <span aria-hidden style={this.styles.fakeListItemText} className="hint--bottom hint--no-animate hint--styled" aria-label={location.name}>
                                                    {location.name}
                                                </span>
                                                <div style={this.styles.listItemText}>
                                                    {location.name}
                                                </div>
                                            </div>
                                        )}/>
                                    </div>
                                ))}
                                <div style={((locations.length > maxCount) && (!showAll || isNarrow)) ? { } : { display: 'none' }}>
                                    <ListItem hoverColor="transparent" innerDivStyle={this.styles.listItemContainer} onClick={() => this._toggle()} secondaryText={(
                                        <div style={Object.assign({}, this.styles.listItem, {  paddingTop: '3px', paddingLeft: '10px' })}>
                                            <span style={showAll ? {} : { display: 'none' } }>... {locations.length - maxCount} less</span>
                                            <span style={showAll ? { display: 'none' } : {} }>... {locations.length - maxCount} more</span>
                                        </div>
                                    )}/>
                                </div>
                        </List>
                    </div>
                </div>
                <Divider inset={true} style={this.styles.divider} />
            </li>
        )
    }
})

export const Sidebar = connect(
    state => ({
        locations: state.locations || {},
        map: state.map,
        sidebarWidth: getSidebarWidth(state),
        layout: state.layout
    })
)(class Sidebar extends Component {
    constructor () {
        super()
        this.styles = {
            divider: {
                backgroundColor: colors.divider,
                marginTop: '22px'
            },
            sidebarContainer: {
                paddingLeft: '22px',
                paddingRight: '8px'
            }
        }
    }
    render () {
        return (
            <div>
                <Divider style={this.styles.divider} />
                <div style={this.styles.sidebarContainer}>
                    <ul>
                        {Object.keys(this.props.locations).map(categoryKey => {
                            let category = this.props.locations[categoryKey]
                            return (
                                <Category layout={this.props.layout} category={category} sidebarWidth={this.props.sidebarWidth} map={this.props.map} key={'category-' + categoryKey}/>
                            )
                        })}
                    </ul>
                </div>
            </div>
        )
    }
})
