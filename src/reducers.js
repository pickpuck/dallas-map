import { drawerReducer } from './Drawer'
import { locationsVisibilityReducer } from './Sidebar'
import { offsetCenter } from './util'
import { DEFAULT_CENTER, BREAKPOINT } from './constants'
import { getIcon } from './sprite'

function mapCenterReducer (state, action) {
    switch (action.type) {
        case 'UPDATE_MAP_CENTER':
            let newState = {}
            let { center, offsetX, map } = action
            newState.center = offsetCenter({
                center,
                offsetX,
                map
            })
            return Object.assign({}, state, newState)
        default:
            return state
    }
}

function keepMapCenteredReducer (state, action) {
    switch (action.type) {
        case 'KEEP_MAP_CENTERED':
            if (state.map) {
                let center
                if (state.center && state.center.lat) {
                    center = {
                        lat: state.center.lat(),
                        lng: state.center.lng()
                    }
                } else {
                    center = DEFAULT_CENTER
                }
                let newCenter = offsetCenter({
                    center: {
                        lat: center.lat,
                        lng: center.lng + .000001 // force update
                    },
                    map: state.map
                })
                return Object.assign({}, state, { center: newCenter })
            }
            return state
        default:
            return state
    }
}

function mapLoadedReducer (state, action) {
    switch (action.type) {
        case 'MAP_LOADED':
            let newState = {}
            newState.map = action.map
            return Object.assign({}, state, newState)
        default:
            return state
    }
}

function mapZoomReducer (state, action) {
    switch (action.type) {
        case 'UPDATE_MAP_ZOOM':
            let newState = {}
            newState.zoom = action.zoom
            return Object.assign({}, state, newState)
        default:
            return state
    }
}

function sidebarToggleReducer (state, action) {
    switch (action.type) {
        case 'SIDEBAR_OPEN':
            return Object.assign({}, state, {sidebarIsOpen: true})
        case 'SIDEBAR_CLOSE':
            return Object.assign({}, state, {sidebarIsOpen: false})
        default:
            return state
    }
}

function locationSelectedReducer (state, action) {
    switch (action.type) {
        case 'LOCATION_SELECTED':
            let newLocations = {}
            Object.keys(state.locations).forEach(locationsKey => {
                newLocations[locationsKey] = Object.assign({}, state.locations[locationsKey])
                newLocations[locationsKey].locations = state.locations[locationsKey].locations.map(location => {
                    if (location.id === action.marker.id) {
                        location.icon = getIcon({categoryId: location.categoryId, active: true})
                    } else {
                        location.icon = getIcon({categoryId: location.categoryId})
                    }
                    return location
                })
            })
            return Object.assign({}, state, { locations: newLocations })
        default:
            return state
    }
}


function locationDeselectedReducer (state, action) {
    switch (action.type) {
        case 'LOCATION_DESELECTED':
            let newLocations = {}
            Object.keys(state.locations).forEach(locationsKey => {
                newLocations[locationsKey] = state.locations[locationsKey]
                newLocations[locationsKey].locations = state.locations[locationsKey].locations.map(location => {
                    location.icon = getIcon({categoryId: location.categoryId})
                    return location
                })
            })
            return Object.assign({}, state, { locations: newLocations })
        default:
            return state
    }
}

function layoutReducer (state, action) {
    switch (action.type) {
        case 'LAYOUT_DIMENSIONS_UPDATE':
            let newState = { layout: Object.assign({}, action.layout) }
            if (state.map && state.layout && (state.layout.width > BREAKPOINT && action.layout.width <= BREAKPOINT)) {
                newState.center = offsetCenter({
                    center: {lat: state.center.lat(),lng: state.center.lng()},
                    offsetX: -50, // difference in sidebar widths / 2
                    map: state.map
                })
            }
            if (state.map && state.layout && (state.layout.width < BREAKPOINT && action.layout.width >= BREAKPOINT)) {
                newState.center = offsetCenter({
                    center: {lat: state.center.lat(),lng: state.center.lng()},
                    offsetX: 55, // difference in sidebar widths / 2
                    map: state.map
                })
            }
            return Object.assign({}, state, newState)
        default:
            return state
    }
}

function locationDataReducer (state, action) {
    switch (action.type) {
        case 'LOCATION_DATA':
            return Object.assign({}, state, { locations: action.data })
        default:
            return state
    }
}

const allReducers = [
    drawerReducer,
    locationsVisibilityReducer,
    locationSelectedReducer,
    locationDeselectedReducer,
    mapCenterReducer,
    mapZoomReducer,
    mapLoadedReducer,
    sidebarToggleReducer,
    keepMapCenteredReducer,
    layoutReducer,
    locationDataReducer
]

export default (state, action) => {
    return allReducers.reduce(function (state, reducer) {
        return reducer(state, action)
    }, state)
}
