import { DEFAULT_ZOOM, DETAIL_ZOOM } from './constants'

export const getSidebarWidth = (state) => (
    (state.layout && (
        state.layout.width < 600
    )) ? 208 : 308
)

// http://stackoverflow.com/q/10656743
export const offsetCenter = ({map, center, offsetX, offsetY}) => {

    // latlng is the apparent centre-point
    // offsetX is the distance you want that point to move to the right, in pixels
    // offsetY is the distance you want that point to move upwards, in pixels
    // offset can be negative
    // offsetX and offsetY are both optional

    let currentZoom = map.getZoom()
    let zoom = DETAIL_ZOOM
    if (currentZoom !== DEFAULT_ZOOM && currentZoom !== DETAIL_ZOOM) {
        zoom = currentZoom
    }
    let scale = Math.pow(2, zoom) //zoom
    let latlng = new window.google.maps.LatLng({lat:center.lat, lng:center.lng})
    let worldCoordinateCenter = map.getProjection().fromLatLngToPoint(latlng)
    let pixelOffset = new window.google.maps.Point((offsetX/scale) || 0,(offsetY/scale) ||0)

    let worldCoordinateNewCenter = new window.google.maps.Point(
        worldCoordinateCenter.x - pixelOffset.x,
        worldCoordinateCenter.y + pixelOffset.y
    )

    let newCenter = map.getProjection().fromPointToLatLng(worldCoordinateNewCenter)
    return newCenter

}
